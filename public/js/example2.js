// import tinysynth.js
window.addEventListener('load', initExample2);

function initExample2() {
  let synth = new Tinysynth();
  let note = null;
  const btn = document.querySelector('#btnExample2');
  btn.addEventListener('mousedown', () => {
    note = synth.noteOn(440, duration=500);
  });
}
