'use strict';
// Adapted from
// https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Simple_synth
// TODO: See this: https://webaudio.github.io/web-midi-api/#a-simple-monophonic-sine-wave-midi-synthesizer

class Tinysynth {
  debug = true;
  constructor() {
    this.noteFreqs = []
    this.oscList = [];
    this.mainGainNode = null;
    try {
      // Init AudioContext
      this.context = new (window.AudioContext || window.webkitAudioContext)();
    } catch(e) {
      throw 'Web Audio API is not supported in this browser';
    }
    this.createNoteTable();
    this.initOscilators();
  }


  // PUBLIC
  setGain(gain) {
    // min: 0, max: 1
    this.mainGainNode.gain.value = gain;
  }


  noteOn(note = null, duration = null) {
    // Returns the oscillator
    let number = null;
    let freq = null;
    if (typeof(note) == 'number') {
      freq = note;
    } else {
      freq = this.midiToFreq(this.noteNameToMidiNote(note));
    }
    let noteObj = new Note(this, freq);
    if (duration != null) {
      // Play note for specified duration
      setTimeout(
        (
          (noteObj) => { noteObj.noteOff()}
        ).bind(null, noteObj),
        duration,
      );
    }
    return noteObj;
  }


  midiNoteOn(midiKey, velocity) {
    let freq = this.noteFreqs[midiKey];
    let osc = this.oscList[midiKey].osc;
    osc.frequency.value = freq;
    this.oscList[midiKey].gainNode.gain.value = velocity/127;
  }


  midiNoteOff(midiKey) {
    this.oscList[midiKey].gainNode.gain.value = 0;
  }


  //// PRIVATE
  initOscilators() {
    // Main volume
    this.mainGainNode = this.context.createGain();
    this.mainGainNode.connect(this.context.destination);
    this.mainGainNode.gain.value = 0.1;
    for (let i=0; i<127; i++) {
      // Notes that can play, each has own oscillator and GainNode (amp env)
      let gainNode = this.context.createGain();
      gainNode.gain.value = 0;
      gainNode.connect(this.mainGainNode);
      let osc = this.context.createOscillator();
      osc.type = 'sine';
      osc.connect(gainNode);
      osc.start();
      this.oscList[i] = {
        osc: osc,
        gainNode: gainNode,
      };
    }
  }


  createNoteTable() {
    for (var i = 0; i <= 127; i++) {
      this.noteFreqs.push(+this.midiToFreq(i).toFixed(2));
    }
  }


  noteNameToMidiNote(noteName) {
    const names = [ 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' ];
    //let octave = 4; // Default octave, if not specified
    let octave = noteName.substr(1);
    let note = noteName[0];
    if (noteName[1] == '#' || noteName[1] == 'b') {
      // If second char is # or b then the rest is the octave
      note = noteName.substr(0, 2);
      octave = noteName.substr(2);
    }
    if (octave == '') {
      // If no octave specified, default to octave 4
      octave = 4;
    }
    octave = parseInt(octave);
    let midiNote = (octave+1)*names.length + names.indexOf(note);
    return midiNote;
  };


  midiToFreq(midiNote) {
    // P_n = 440*2^((n-49)/12)
    // Ref: https://en.wikipedia.org/wiki/Equal_temperament#Calculating_absolute_frequencies
    // Ref: https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies
    let freq = 440 * ( Math.pow(2, (midiNote-69)/12) );
    return freq;
  }
}; // class Tinysynth


class Note {
  // For single note
  // Note: accepts only freq
  parent = null
  osc = null
  constructor(prnt, freq) {
    //this.parent = parent;
    // Main volume
    prnt.mainGainNode = prnt.context.createGain();
    prnt.mainGainNode.connect(prnt.context.destination);
    let gainNode = prnt.context.createGain();
    gainNode.gain.value = 0.1; // TODO
    gainNode.connect(prnt.mainGainNode);
    let osc = prnt.context.createOscillator();
    osc.type = 'sawtooth';
    osc.connect(gainNode);
    osc.frequency.value = freq;
    osc.start();
    this.osc = osc;
  }

  noteOff() {
    this.osc.stop();
  }

  setFrequency(freq) {
    this.osc.frequency.value = freq;
  }
}; // class Note
