// import tinysynth.js
console.log('test');
let synth = new Tinysynth();

let midiNotes = [
  // midiNote  noteName
  [ 0 , 'C-1'  ],
  [ 1 , 'C#-1' ],
  [ 2 , 'D-1'  ],
  [ 3 , 'D#-1' ],
  [ 4 , 'E-1'  ],
  [ 5 , 'F-1'  ],
  [ 6 , 'F#-1' ],
  [ 7 , 'G-1'  ],
  [ 8 , 'G#-1' ],
  [ 9 , 'A-1'  ],
  [ 10, 'A#-1' ],
  [ 11, 'B-1'  ],
  [ 12, 'C0'   ],
  [ 13, 'C#0'  ],
  [ 14, 'D0'   ],
  [ 15, 'D#0'  ],
  [ 16, 'E0'   ],
  [ 17, 'F0'   ],
  [ 18, 'F#0'  ],
  [ 19, 'G0'   ],
  [ 20, 'G#0'  ],
  [ 21, 'A0'   ],
  [ 22, 'A#0'  ],
  [ 23, 'B0'   ],
  [ 24, 'C1'   ],
  [ 25, 'C#1'  ],
  //
  [ 36, 'C2'   ],
  //
  [ 60, 'C4'   ],
  [ 61, 'C#4'  ],
  [ 62, 'D4'   ],
  [ 63, 'D#4'  ],
  [ 64, 'E4'   ],
  [ 65, 'F4'   ],
  [ 66, 'F#4'  ],
  [ 67, 'G4'   ],
  [ 68, 'G#4'  ],
  [ 69, 'A4'   ],
  [ 70, 'A#4'  ],
  [ 71, 'B4'   ],
  [ 72, 'C5'   ],
  // No octave specified: default to octave 4
  [ 60, 'C'   ],
  [ 61, 'C#'  ],
  [ 62, 'D'   ],
  [ 63, 'D#'  ],
  [ 64, 'E'   ],
  [ 65, 'F'   ],
  [ 66, 'F#'  ],
  [ 67, 'G'   ],
  [ 68, 'G#'  ],
  [ 69, 'A'   ],
  [ 70, 'A#'  ],
  [ 71, 'B'   ],
];

for (noteObj of midiNotes) {
  let midiNote = noteObj[0];
  let noteName = noteObj[1];
  //console.log(noteObj);
  let result = synth.noteNameToMidiNote(noteName);
  if (result == midiNote) {
    console.log('PASS:', midiNote, noteName);
  } else {
    console.log('FAIL:', midiNote, noteName, 'got:', result);
  }
}
