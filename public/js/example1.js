// import tinysynth.js
window.addEventListener('load', initExample1);

function initExample1() {
  const synth = new Tinysynth();
  let note = null;
  const btn = document.querySelector('#btnExample1');
  btn.addEventListener('mousedown', () => {
    note = synth.noteOn(440);
  });
  btn.addEventListener('mouseup', () => {
    note.noteOff();
  });
}
