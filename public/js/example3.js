// import tinysynth.js
window.addEventListener('load', initExample3);
let noteB = null;

function initExample3() {
  let synth = new Tinysynth();

  // A#4
  let noteA = null;
  const btnA = document.querySelector('#btnExample3a');
  btnA.addEventListener('mousedown', () => {
    noteA = synth.noteOn('A#4');
  });
  btnA.addEventListener('mouseup', () => {
    noteA.noteOff();
  });

  // A#
  //let noteB = null;
  const btnB = document.querySelector('#btnExample3b');
  btnB.addEventListener('mousedown', () => {
    noteB = synth.noteOn('A#');
  });
  btnB.addEventListener('mouseup', () => {
    noteB.noteOff();
  });
}
