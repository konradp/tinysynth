# tinysynth
<https://konradp.gitlab.io/tinysynth>

## Run
```BASH
npm install
npm start
```
Navigate in web browser to http://localhost:3000/
